﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAiming : MonoBehaviour {

    public Camera FPSCamera;
    public bool lockAiming = false;
    public GunAimingData gunAimingConfig = new GunAimingData(30f, 60f, Vector3.zero, Vector3.zero, 10f);
    private Vector3 smoother;
    private float Fsmoother;
    private bool isAiming = false;

	
	// Update is called once per frame
	void Update () {

        Aiming();

        float aimSpeed = gunAimingConfig.aimingSpeed * Time.deltaTime;

        if (isAiming == true)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, gunAimingConfig.aimingPos, ref smoother, aimSpeed);
            FPSCamera.fieldOfView = Mathf.SmoothDamp(FPSCamera.fieldOfView, gunAimingConfig.aimingZoom, ref Fsmoother, aimSpeed);
        }
        else
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, gunAimingConfig.hipPos, ref smoother, aimSpeed);
            FPSCamera.fieldOfView = Mathf.SmoothDamp(FPSCamera.fieldOfView, gunAimingConfig.hipZoom, ref Fsmoother, aimSpeed);
        }
	}

    private void Aiming()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            isAiming = (lockAiming) ? !isAiming : true;
        }
        else if (Input.GetButtonUp("Fire2") && !lockAiming)
        {
            isAiming = false;
        }
    }

    [System.Serializable]
    public struct GunAimingData
    {
        public float aimingZoom;
        public float hipZoom;
        public Vector3 aimingPos;
        public Vector3 hipPos;
        public float aimingSpeed;

        public GunAimingData(float aimingZoom, float hipZoom, Vector3 aimingPos, Vector3 hipPos, float aimingSpeed)
        {
            this.aimingZoom = aimingZoom;
            this.hipZoom = hipZoom;
            this.aimingPos = aimingPos;
            this.hipPos = hipPos;
            this.aimingSpeed = aimingSpeed;
        }
    }
}
