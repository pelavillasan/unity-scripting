﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class PlayerFPSController : MonoBehaviour {

    private CharacterMovement characterMovement;

	// Use this for initialization
	void Start () {
        GameObject.Find("Capsule").GetComponent<MeshRenderer>().enabled = false;

        characterMovement = GetComponent<CharacterMovement>();
	}
	
	// Update is called once per frame
	void Update () {

        movement();

	}

    private void movement (){

        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        bool dash = Input.GetButton("Dash");
        bool jump = Input.GetButtonDown("Jump");

        characterMovement.moveCharacter(hMovement, vMovement, jump, dash);
    }
}
