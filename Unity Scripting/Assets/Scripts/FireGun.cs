﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGun : MonoBehaviour {

    [Header("Gun Specs")]
    public FireGunData gunData = new FireGunData(10f, 0.1f, 200f, 700f, 30, 1f);

    private int currentAmmo;
    private float firingTimer;
    private bool isRealoading = false;

    public GameObject bullet_hole;
    public Camera fpsCamera;
    private RaycastHit hit;
    private Ray ray;
    private Recoiler gunRecoiler;
    private Recoiler camRecoiler;

    private void Awake()
    {
        gunRecoiler = GetComponentInParent<Recoiler>();
        camRecoiler = fpsCamera.GetComponentInParent<Recoiler>();

        currentAmmo = gunData.magazineCapacity;
    }

    // Update is called once per frame
    void Update()
    {
        if (isRealoading)
            return;

        if (currentAmmo <= 0)
        {
            StartCoroutine(reload());
            return;
        }

        shoot();
    }

    private void shoot()
    {
        if (Time.time >= firingTimer && Input.GetButton("Fire1"))
        {
            firingTimer = Time.time + 60f / gunData.fireRate;

            currentAmmo--;

            Vector2 screenCenterPoint = new Vector2(Screen.width / 2, Screen.height / 2);
            gunRecoiler.recoil += gunData.recoil;
            camRecoiler.recoil += gunData.recoil;

            ray = fpsCamera.ScreenPointToRay(screenCenterPoint);

            if (Physics.Raycast(ray, out hit, gunData.range))
            {
                Vector3 bulletHolePosition = hit.point + hit.normal * 0.01f;

                Quaternion bulletHoleRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                GameObject hole = Instantiate(bullet_hole, bulletHolePosition, bulletHoleRotation);
            }
        }
    }

    private IEnumerator reload()
    {
        isRealoading = true;

        yield return new WaitForSeconds(gunData.reloadTime);

        currentAmmo = gunData.magazineCapacity;
        isRealoading = false;
    }

    [System.Serializable]
    public struct FireGunData
    {
        public float power;
        public float recoil;
        public float fireRate;
        public float range;
        public int magazineCapacity;
        public float reloadTime;

        public FireGunData(float power, float recoil, float fireRate, float range, int magazineCapacity, float reloadTime)
        {
            this.power = power;
            this.recoil = recoil;
            this.fireRate = fireRate;
            this.range = range;
            this.magazineCapacity = magazineCapacity;
            this.reloadTime = reloadTime;
        }
    }
}
