//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("CapsuleMovement_U", "")]
public class CapsuleMovement_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 Directions = new Vector3( (float)0, (float)0, (float)1 );
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_7_System_Single = (float) 0;
   System.Single local_8_System_Single = (float) 0;
   public System.Single Speed = (float) 10;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_9 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_0 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_0 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_0 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_XMin_0 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_XMax_0 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_0 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_YMin_0 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_YMax_0 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_0 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_ZMin_0 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_ZMax_0 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_0;
   bool logic_uScriptAct_ClampVector3_Out_0 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_2 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_2;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_2;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_2;
   bool logic_uScriptAct_GetDeltaTime_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_3 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_3;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_4 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_4 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_4;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_4;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_4 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_1 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_11 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_9 || false == m_RegisteredForEvents )
      {
         owner_Connection_9 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_1 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_1 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_1 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_1.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_1;
                  component.OnLateUpdate += Instance_OnLateUpdate_1;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_1;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_1 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_1;
               component.OnLateUpdate -= Instance_OnLateUpdate_1;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_1;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_0.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_2.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4.SetParent(g);
      owner_Connection_9 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_1(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_1( );
   }
   
   void Instance_OnLateUpdate_1(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_1( );
   }
   
   void Instance_OnFixedUpdate_1(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_1( );
   }
   
   void Relay_In_0()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("25874d27-8e47-4032-8268-c2a50777f6b4", "Clamp_Vector3", Relay_In_0)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_0 = Directions;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_0.In(logic_uScriptAct_ClampVector3_Target_0, logic_uScriptAct_ClampVector3_ClampX_0, logic_uScriptAct_ClampVector3_XMin_0, logic_uScriptAct_ClampVector3_XMax_0, logic_uScriptAct_ClampVector3_ClampY_0, logic_uScriptAct_ClampVector3_YMin_0, logic_uScriptAct_ClampVector3_YMax_0, logic_uScriptAct_ClampVector3_ClampZ_0, logic_uScriptAct_ClampVector3_ZMin_0, logic_uScriptAct_ClampVector3_ZMax_0, out logic_uScriptAct_ClampVector3_Result_0);
         Directions = logic_uScriptAct_ClampVector3_Result_0;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_0.Out;
         
         if ( test_0 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_1()
   {
      if (true == CheckDebugBreak("3cf4b4bf-5930-4eff-81cb-957277db9f85", "Global_Update", Relay_OnUpdate_1)) return; 
      Relay_In_0();
   }
   
   void Relay_OnLateUpdate_1()
   {
      if (true == CheckDebugBreak("3cf4b4bf-5930-4eff-81cb-957277db9f85", "Global_Update", Relay_OnLateUpdate_1)) return; 
   }
   
   void Relay_OnFixedUpdate_1()
   {
      if (true == CheckDebugBreak("3cf4b4bf-5930-4eff-81cb-957277db9f85", "Global_Update", Relay_OnFixedUpdate_1)) return; 
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("fa6183d1-7ebe-41a6-955e-b1e1de52673c", "Get_Delta_Time", Relay_In_2)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_2.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_2, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_2, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_2);
         local_8_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_2.Out;
         
         if ( test_0 == true )
         {
            Relay_In_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d532941a-1c1a-4898-9080-bae7eab8806c", "Multiply_Vector3_With_Float", Relay_In_3)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3 = Directions;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_3 = local_7_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_3, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_3, out logic_uScriptAct_MultiplyVector3WithFloat_Result_3);
         local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_3;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_3.Out;
         
         if ( test_0 == true )
         {
            Relay_Translate_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc91206f-22cf-4eb0-87c9-1cb23e2b3987", "Multiply_Float", Relay_In_4)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_4 = Speed;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_4 = local_8_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4.In(logic_uScriptAct_MultiplyFloat_v2_A_4, logic_uScriptAct_MultiplyFloat_v2_B_4, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_4, out logic_uScriptAct_MultiplyFloat_v2_IntResult_4);
         local_7_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_4;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4.Out;
         
         if ( test_0 == true )
         {
            Relay_In_3();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f5d9b5ab-6bc0-474c-aefc-3563a5ebe4be", "UnityEngine_Transform", Relay_Translate_11)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_11 = local_10_UnityEngine_Vector3;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_9.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_11);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:Directions", Directions);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "3c8f278d-c304-4201-93d3-782099686be5", Directions);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:Speed", Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "868307de-c306-4114-ab12-fbf244f368f2", Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:7", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5bbedd36-0eb3-4739-b63d-6b378a11fdd1", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:8", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8735cc4f-470c-436c-a7c6-0030e8425670", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:10", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9a943e92-d459-4655-be1a-3f7f5866d497", local_10_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
